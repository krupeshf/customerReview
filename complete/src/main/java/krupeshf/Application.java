package krupeshf;

import krupeshf.model.Customer;
import krupeshf.model.Product;
import krupeshf.model.Review;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.annotation.PostConstruct;
import javax.validation.*;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@SpringBootApplication
public class Application implements CommandLineRunner {

    private static final Logger log = LoggerFactory.getLogger(Application.class);

    public static void main(String args[]) {
        SpringApplication.run(Application.class, args);
    }

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Autowired
    Validator validator;

    /**
     * Initializing the validator only once this way initialize method on all the validator are not called every single time
     */
    @PostConstruct
    public Validator initializeValidator() {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        return factory.getValidator();
    }

    @Override
    public void run(String... strings) throws Exception {
        initiazeTables();
        insertDummyCustomers();
        insertDummyProducts();
        insertDummyReviews();
        logList(queryCustomerByFirstName("Josh"));

        Review r1 = new Review(1, 1, 2.2d, "average");
        Review r2 = new Review(1, 1, -1d, "hello how are you world");
        validateAndInsertReview(r1);

        try {
            validateAndInsertReview(r2);
        } catch (ValidationException ve) {
            log.error(ve.getMessage());
        }

        log.info("Number of reviews between range: " + queryProductById(1).getReviewCountBetweenRange(2.0d, 5.0d));
    }

    private void initiazeTables() {
        initializeCustomerTable();
        initializeProductsTable();
        initializeReviewsTable();
    }

    private void initializeCustomerTable() {
        log.info("Creating customer table");

        jdbcTemplate.execute("DROP TABLE customers IF EXISTS");
        jdbcTemplate.execute("CREATE TABLE customers(" +
                "id SERIAL, first_name VARCHAR(255), last_name VARCHAR(255))");
    }

    private void initializeProductsTable() {
        log.info("Creating product table");

        jdbcTemplate.execute("DROP TABLE products IF EXISTS");
        jdbcTemplate.execute("CREATE TABLE products(id SERIAL, name VARCHAR(255))");
    }

    private void initializeReviewsTable() {
        log.info("Creating review table");

        jdbcTemplate.execute("DROP TABLE reviews IF EXISTS");
        jdbcTemplate.execute("CREATE TABLE reviews (id SERIAL, " +
                "customerId BIGINT, " +
                "productId BIGINT, " +
                "rating DOUBLE, " +
                "review VARCHAR(255)," +
                "foreign key (customerId) references customers(id)," +
                "foreign key (productId) references products(id)" +
                ")");
    }

    private void insertDummyCustomers() {
        // Split up the array of whole names into an array of first/last names
        List<Object[]> splitUpNames = Arrays.asList("John Woo", "Jeff Dean", "Josh Bloch", "Josh Long").stream()
                .map(name -> name.split(" "))
                .collect(Collectors.toList());

        // Use a Java 8 stream to print out each tuple of the list
        splitUpNames.forEach(name -> log.info(String.format("Inserting customer record for %s %s", name[0], name[1])));

        // Uses JdbcTemplate's batchUpdate operation to bulk load data
        jdbcTemplate.batchUpdate("INSERT INTO customers(first_name, last_name) VALUES (?,?)", splitUpNames);
    }

    private void insertDummyProducts() {
        List<Object[]> productNames = Arrays.asList("drill", "sander", "hammer").stream().map(n -> new String[]{n}).collect(Collectors.toList());

        // Uses JdbcTemplate's batchUpdate operation to bulk load data
        jdbcTemplate.batchUpdate("INSERT INTO products(name) VALUES (?)", productNames);
    }

    private void insertDummyReviews() {
        List<Object[]> reviews = Arrays.asList(
                "1;;1;;0.5;;bad drill",
                "2;;3;;3.5;;above average hammer",
                "2;;1;;5;;great drill",
                "2;;2;;5;;awesome sander",
                "3;;1;;2.5;;average drill").stream().map(n -> n.split(";;")).collect(Collectors.toList());

        // Uses JdbcTemplate's batchUpdate operation to bulk load data
        jdbcTemplate.batchUpdate("INSERT INTO reviews(customerId, productId, rating, review) VALUES (?, ?, ?, ?)", reviews);
    }

    private List<Customer> queryCustomerByFirstName(final String firstName) {
        log.info(String.format("Querying for customer records where first_name = '%s':", firstName));
        return jdbcTemplate.query(
                "SELECT id, first_name, last_name FROM customers WHERE first_name = ?", new Object[]{firstName},
                (rs, rowNum) -> new Customer(rs.getLong("id"), rs.getString("first_name"), rs.getString("last_name"))
        );
    }

    private List<Review> queryReviewsByProductId(final long productId) {
        log.info(String.format("Querying for reviews records where productId = '%s':", productId));
        return jdbcTemplate.query(
                "SELECT id, customerId, productId, rating, review FROM reviews WHERE productId = ?", new Object[]{productId},
                (rs, rowNum) -> new Review(rs.getLong("id"),
                        rs.getLong("customerId"),
                        rs.getLong("productId"),
                        rs.getDouble("rating"),
                        rs.getString("review"))
        );
    }

    private Product queryProductById(final long productId) {
        log.info(String.format("Querying for product records where productId = '%s':", productId));
        return jdbcTemplate.query(
                "SELECT id, name FROM products WHERE id = ?", new Object[]{productId},
                (rs, rowNum) -> new Product(rs.getLong("id"), rs.getString("name"))
        ).get(0).setReviews(queryReviewsByProductId(productId));
    }

    private <T> void logList(final List<T> valueList) {
        valueList.forEach(value -> log.info(value.toString()));
    }

    private void validateReview(Review review) {
        Set<ConstraintViolation<Review>> violations = validator.validate(review);
        if (violations.size() > 0) {
            throw new ValidationException(String.join(",\n", violations.stream().map(m -> m.getMessage()).collect(Collectors.toList())));
        }
    }

    private void insertReview(Review review) {
        jdbcTemplate.update("INSERT INTO reviews(customerId, productId, rating, review) VALUES (?, ?, ?, ?)", review.convertReviewForInsertion());
    }

    private void validateAndInsertReview(Review review) {
        validateReview(review);
        insertReview(review);
    }

}