package krupeshf.validations;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class CurseWordsValidator implements ConstraintValidator<NoCurseWords, String> {

    private String curseWordsRegEx;
    @Override
    public void initialize(NoCurseWords constraintAnnotation) {
        try {
            String pathForWords = System.getProperty("user.dir") + File.separator + "words.txt";

            // reads all lines in file, joins with | and then creates a regex
            // example of regEx -->> .*(bello|world).*
            // easier to validate
            curseWordsRegEx = ".*(" + String.join("|", Files.readAllLines( Paths.get(pathForWords))) + ").*";
        } catch (IOException e) {
            curseWordsRegEx = "";
        }
    }

    @Override
    public boolean isValid(String review, ConstraintValidatorContext constraintValidatorContext) {
        return !review.matches(curseWordsRegEx);
    }
}
