package krupeshf.validations;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = CurseWordsValidator.class)
@Target({ElementType.METHOD, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface NoCurseWords {

    String message() default "No Curse Words please!!!";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
