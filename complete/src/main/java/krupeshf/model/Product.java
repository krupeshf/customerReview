package krupeshf.model;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Product {
    private long id;
    private String name;
    private List<Review> reviews;

    public Product(long id, String name) {
        this.id = id;
        this.name = name;
        this.reviews = new ArrayList<>();
    }

    public void addReview(Review review) {
        this.reviews.add(review);
    }

    // ToBe used in future if needed
    private List<Review> getReviewsBetweenRange(final Double min, final Double max) {
        return this.reviews.parallelStream().filter(review -> review.isInRange(min, max)).collect(Collectors.toList());
    }

    public long getReviewCountBetweenRange(final Double min, final Double max) {
        return this.reviews.parallelStream().filter(review -> review.isInRange(min, max)).count();
    }

    @Override
    public String toString() {
        return String.format(
                "Product[id=%d, name='%s']",
                id, name);
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Review> getReviews() {
        return reviews;
    }

    public Product setReviews(List<Review> reviews) {
        this.reviews = reviews;
        return this;
    }
}

