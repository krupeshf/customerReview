package krupeshf.model;

import krupeshf.validations.NoCurseWords;

import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;

public class Review {
    private long id;
    private long customerId;
    private long productId;

    @NoCurseWords
    private String reviewText;

    @DecimalMin(value = "0.0", message = "Ratings cannot be negative")
    @DecimalMax(value = "5.0", message = "Ratings cannot be more than 5")
    private Double rating;

    public Review(long id, long customerId, long productId, Double rating, String reviewText) {
        this(customerId, productId, rating, reviewText);
        this.id = id;
    }

    public Review(long customerId, long productId, Double rating, String reviewText) {
        this.customerId = customerId;
        this.productId = productId;
        this.rating = rating;
        this.reviewText = reviewText;
    }

    public boolean isInRange(final Double min, final Double max) {
        return this.rating >= min && this.rating <= max;
    }

    @Override
    public String toString() {
        return String.format(
                "Review[id=%d, customerId='%s', productId='%s', rating='%s', reviewText='%s']",
                id, customerId, productId, rating, reviewText);
    }

    public Object[] convertReviewForInsertion() {
        return new Object[]{this.customerId, this.productId, this.rating, this.reviewText};
    }

}

